# Bootloader for MNT Reform LS1028A Module

To flash the result to an SD card:

`sudo dd if=flash.bin of=/dev/sdX conv=notrunc bs=512 seek=8 status=progress`
