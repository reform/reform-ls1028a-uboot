#!/bin/bash

set -xe

if [ -z ${SOURCE_DATE_EPOCH:+x} ] && git -C . rev-parse 2>/dev/null; then
	export SOURCE_DATE_EPOCH=$(git log -1 --format=%ct)
fi
export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm64
export LDFLAGS=--no-warn-rwx-segments

# build Reset Configuration Word
cd rcw/ls1028a-mnt-reform2
make
cd ../..

# build u-boot
# TODO: customize dts
git clone --branch v2023.10-rc4 --depth 1 https://github.com/u-boot/u-boot.git
cd u-boot
#git checkout 252592214f79d8206c3cf0056a8827a0010214e0
cp ../u-boot-config ./.config
for p in ../patches/*.patch
do
    patch -p1 <"$p"
done
make -j$(nproc)
cd ..

# build ARM Trusted Firmware
git clone https://source.mnt.re/reform/reform-ls1028a-atf.git atf

export DDR_BIST=no
export DDR_DEBUG=yes
export DDR_PHY_DEBUG=yes

cd atf
make -j$(nproc) PLAT=ls1028ardb fip pbl BOOT_MODE=sd RCW=../rcw/ls1028a-mnt-reform2/R_PSPH_0xb8be/rcw_1500_sdboot.bin BL33=../u-boot/u-boot-dtb.bin
cd ..

cp atf/build/ls1028ardb/release/bl2_sd.pbl flash.bin
dd if=atf/build/ls1028ardb/release/fip.bin of=flash.bin bs=512 seek=2040
